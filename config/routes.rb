Nattogdag2::Application.routes.draw do
  resources :teams, :only => [:update, :create, :show, :new]
  resources :players, :only => [:update, :create, :show, :index]
  root :to => 'static_pages#start'
  get "static_pages/export"
  match 'export' => 'static_pages#export'
  match 'teams/:id' => 'teams#show'
end
