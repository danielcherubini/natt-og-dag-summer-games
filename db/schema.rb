# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130524121753) do

  create_table "players", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.integer  "team_id"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.boolean  "confim",     :default => false
    t.string   "teamslug"
    t.string   "slug"
  end

  add_index "players", ["slug"], :name => "index_players_on_slug"
  add_index "players", ["team_id", "created_at"], :name => "index_players_on_team_id_and_created_at"

  create_table "teams", :force => true do |t|
    t.string   "teamgroup"
    t.string   "teamname"
    t.string   "name"
    t.string   "email"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "slug"
  end

  add_index "teams", ["slug"], :name => "index_teams_on_slug"

end
