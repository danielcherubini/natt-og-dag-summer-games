class AddSlugsToPlayer < ActiveRecord::Migration
  def change
    add_column :players, :teamslug, :string
    add_column :players, :slug, :string
    add_index :players, :slug
  end
end
