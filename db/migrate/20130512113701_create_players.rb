class CreatePlayers < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.string :name
      t.string :email
      t.integer :team_id

      t.timestamps
    end
    add_index :players, [:team_id, :created_at]
  end
end
