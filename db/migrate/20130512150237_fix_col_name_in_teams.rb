class FixColNameInTeams < ActiveRecord::Migration
  def up
  	rename_column :teams, :leader, :name
  	rename_column :teams, :leaderemail, :email
  end

  def down
  end
end
