class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :teamgroup
      t.string :teamname
      t.string :leader
      t.string :leaderemail

      t.timestamps
    end
  end
end
