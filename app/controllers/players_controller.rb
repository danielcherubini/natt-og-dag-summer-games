# encoding: UTF-8
class PlayersController < ApplicationController
  # GET /players
  # GET /players.json
  def index
    @players = Player.all
    @teams = Team.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @players }
      format.csv { send_data @players.as_csv }
      format.xls # { send_data @players.to_csv(col_sep: "\t") }
    end
  end

  # GET /players/1
  # GET /players/1.json
  def show
    @player = Player.find_by_slug!(params[:id].split("/").last)

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @player }
    end
  end

  # GET /players/new
  # GET /players/new.json
  def new
    @player = Player.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @player }
    end
  end

  # GET /players/1/edit
  def edit
    @player = Player.find_by_slug!(params[:id].split("/").last)

  end

  # POST /players
  # POST /players.json
  def create
    @player = Player.new(params[:player])
    @team = Team.find_by_id(params[:team_id])

    respond_to do |format|
      if @player.save
        UserMailer.player_confirmation(@player).deliver
        format.html { redirect_to "/teams/#{@player.teamslug}", notice: 'Bra valg, men du burde nok inviter enda en!  ' }
        format.json { render json: @player, status: :created, location: @player }
      else
        format.html { redirect_to "/teams/#{@player.teamslug}", notice: 'Email already taken.' }
        format.json { render json: @player.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /players/1
  # PUT /players/1.json
  def update
    @player = Player.find_by_slug!(params[:id].split("/").last)
    # @team = Team.find_by_slug!(params[:id].split("/").last)
    
    respond_to do |format|
      if @player.update_attributes(params[:player])
        format.html { redirect_to "/teams/#{@player.teamslug}", notice: 'Player was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @player.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /players/1
  # DELETE /players/1.json
  def destroy
    @player = Player.find_by_slug!(params[:id].split("/").last)
    @player.destroy

    respond_to do |format|
      format.html { redirect_to players_url }
      format.json { head :no_content }
    end
  end
end
