class StaticPagesController < ApplicationController
  def start
    
  end

	def export
		@players = Player.all
    @teams = Team.all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @players }
      format.csv { send_data @players.as_csv }
      format.xls # { send_data @players.to_csv(col_sep: "\t") }
    end
	end

end
