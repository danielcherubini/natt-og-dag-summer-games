class Player < ActiveRecord::Base
  attr_accessible :email, :name, :team_id, :confim, :slug, :teamslug
  validates :team_id, presence: true
  belongs_to :team

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence:   true,
                    format:     { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }

  before_validation :generate_slug

  def to_param
    slug.parameterize
  end
  def generate_slug
    self.slug ||= "#{team_id}-#{id}-#{name}".parameterize
  end

  def self.as_csv
    CSV.generate do |csv|
      csv << column_names
      all.each do |item|
        csv << item.attributes.values_at(*column_names)
      end
    end
  end

end
