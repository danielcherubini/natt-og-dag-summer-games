class Team < ActiveRecord::Base
  attr_accessible :name, :email, :teamgroup, :teamname
  has_many :players, dependent: :destroy
  accepts_nested_attributes_for :players

  validates :teamgroup, presence: true, uniqueness: true
  validates :teamname, presence:   true, uniqueness: { case_sensitive: false }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence:   true,
                    format:     { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  validates :name, presence: true
  validates :slug, uniqueness: true, presence: true


  before_validation :generate_slug

  def to_param
	  slug.parameterize
	end
  def generate_slug
  self.slug ||= teamname.parameterize
end
end
