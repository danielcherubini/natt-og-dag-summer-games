class UserMailer < ActionMailer::Base
  default from: "sommerleker@nattogdag.no"

  def signup_confirmation(team)
    @team = team

    mail to: team.email, subject: "Gratulerer , du er helt klart med videre!"
  end
  def player_confirmation(player)
    @player = player

    mail to: player.email, subject: "Invitasjon fra lagkapteinen"
  end
end
